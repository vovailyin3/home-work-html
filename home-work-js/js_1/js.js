window.addEventListener("DOMContentLoaded", function() {
    const inp = document.querySelector(".in"),
        out = document.querySelectorAll(".out");
    inp.addEventListener("input", function() {
        inp.value.split(/\s+/).forEach(function(text,i) {
            out[i] && (out[i].value = text)
        });
    })
});