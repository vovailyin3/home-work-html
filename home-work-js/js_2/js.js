function someAction(x, y, someCallback) {
    return someCallback(x, y);
}

function calcProduct(x, y) {
    return x * y;
}

function calcSum(x, y) {
    return x + y;
}

alert(someAction(5, 15, calcProduct));
alert(someAction(5, 15, calcSum));