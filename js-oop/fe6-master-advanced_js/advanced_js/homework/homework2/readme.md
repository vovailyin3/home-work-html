## Задание

Напишите функцию calcMostRepeatWord(phrase), которая будет принимать в качества аргумента строку текста (она может быть какой угодно большой), и возвращать 2 значения: самое часто встречающееся в этой строке слово и сколько раз оно встречается. Результат работы функции присвоить 2м переменным: popularWord и popularWordCount.

   
#### Литература:
- [Деструктуризация](https://habr.com/ru/post/341500/)
- [Строки в JS](https://learn.javascript.ru/string#metody-i-svoystva)
- [Методы массивов](https://learn.javascript.ru/array-methods)
