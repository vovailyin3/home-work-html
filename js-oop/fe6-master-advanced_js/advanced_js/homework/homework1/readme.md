## Задание

Напишите функцию createUser, которая из полученных аргументов создает объект Пользователь.

#### Технические требования:
В качестве аргументов функция должна принимать:
- имя пользователя;
- фамилию пользователя;
- все остальные аргументы - это строки вида: "имяСвойства: значениеСвойства", которые должны превратится в свойства объекта после их разделения (имяСвойтва и значениеСвойства разделены двоеточием с пробелом).


#### Пример:
   ```javascript
    const user1 = createUser("Золар", "Золотая Молния", "vid: тэнкрис", "age: 180", "height: 210", "status: глава Западного клана Мескии");
    console.log(user1);
    /**
    Выведет:
    user1 = {
        name: "Золар",
        "last name": "Золотая Молния",
        vid: "тэнкрис",
        age: 180,
        height: 210,
        status: "глава Западного клана Мескии"
    } 
    */
  
   ```
   
#### Литература:
- [ES6: Операторы Spread и Rest](http://jsraccoon.ru/es6-spread-rest)
- [Деструктуризация](https://habr.com/ru/post/341500/)
- [Деструктуризация](https://learn.javascript.ru/destructuring)
