<?php 
header('Access-Control-Allow-Origin: *'); 
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');

echo "Fiona Gallagher is a central character and in the difficult position of being the oldest Gallagher child. Since she was 16 years old and her mother walked out, Fiona has taken care of her siblings and dysfunctional, alcoholic father, Frank. Despite how stressful and chaotic her life is, Fiona meets every challenge head on. Her siblings and father often turn to her when they get into trouble with the law!";