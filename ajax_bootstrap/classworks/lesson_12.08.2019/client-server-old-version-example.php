<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
</head>

<body>

    <section class="section">

        <div class="container">
            <!--Section heading-->
            <h2 class="section-heading h1 pt-4">Contact us</h2>
            <!--Section description-->
            <p class="section-description">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
                matter of hours to help you.</p>

            <div class="row">

                <!--Grid column-->
                <div class="col-md-8 col-xl-9">
                    <form id="contact-form" name="contact-form" action="feedback-old.php" method="POST">

                        <!--Grid row-->
                        <div class="row">

                            <!--Grid column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                    <input type="text" id="name" name="name" class="form-control">
                                    <label for="name" class="">Your name</label>
                                </div>
                            </div>
                            <!--Grid column-->

                            <!--Grid column-->
                            <div class="col-md-6">
                                <div class="md-form">
                                    <input type="text" id="email" name="email" class="form-control">
                                    <label for="email" class="">Your email</label>
                                </div>
                            </div>
                            <!--Grid column-->

                        </div>
                        <!--Grid row-->

                        <!--Grid row-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="md-form">
                                    <input type="text" id="subject" name="subject" class="form-control">
                                    <label for="subject" class="">Subject</label>
                                </div>
                            </div>
                        </div>
                        <!--Grid row-->

                        <!--Grid row-->
                        <div class="row">

                            <!--Grid column-->
                            <div class="col-md-12">

                                <div class="md-form">
                                    <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
                                    <label for="message">Your message</label>
                                </div>

                                <div class="center-on-small-only">
                                    <input type="submit" class="btn btn-primary" value="Send">
                                </div>
                                <div class="status">
                                    <?php if($_REQUEST["response"]) {
                                            if($_REQUEST["response"] === "error") {
                                                echo "<div class='alert alert-danger' role='alert'>Произошла ошибка! Попробуйте пожалуйста еще раз</div>";
                                            }
                                            else {
                                                echo "<div class='alert alert-success' role='alert'>Сообщение успешно отправленно, спасибо!</div>";
                                            }
                                        }
                                    ?>
                                </div>

                            </div>
                        </div>
                        <!--Grid row-->

                    </form>


                </div>
                <!--Grid column-->

            </div>
        </div>



    </section>
    <!--Section: Contact v.2-->

</body>

</html>
